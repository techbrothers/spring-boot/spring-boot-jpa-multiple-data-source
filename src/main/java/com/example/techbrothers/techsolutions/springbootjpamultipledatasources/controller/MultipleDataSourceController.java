/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.techbrothers.techsolutions.springbootjpamultipledatasources.controller;

import com.example.techbrothers.techsolutions.springbootjpamultipledatasources.dao.MutipleDataSourceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author chiru
 */
@RestController
public class MultipleDataSourceController {
    
    @Autowired
    MutipleDataSourceDao mutipleDataSourceDao;
    
    
    @GetMapping("/data")
    public void getDataFromDatabase(){
        mutipleDataSourceDao.getData();
    }
}
