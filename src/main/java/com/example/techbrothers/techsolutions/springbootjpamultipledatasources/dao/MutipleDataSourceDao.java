/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.techbrothers.techsolutions.springbootjpamultipledatasources.dao;

import com.example.techbrothers.techsolutions.springbootjpamultipledatasources.domain.User;
import com.example.techbrothers.techsolutions.springbootjpamultipledatasources.domain.User2;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import org.springframework.stereotype.Service;

/**
 *
 * @author chiru
 */
@Service
public class MutipleDataSourceDao {
    
    @PersistenceContext(unitName = "connection1")
    EntityManager entityManager1;
    
     @PersistenceContext(unitName = "connection2")
    EntityManager entityManager2;
    
    @Transactional
    public void getData(){
        StoredProcedureQuery procedureQuery= entityManager1.createNamedStoredProcedureQuery("procedure1");
        List<User> userList = procedureQuery.getResultList();
        userList.stream().forEach( user -> System.out.println(""+ user));
        
        StoredProcedureQuery procedureQuery2= entityManager2.createNamedStoredProcedureQuery("procedure2");
        List<User2> userList2 = procedureQuery2.getResultList();
        userList2.stream().forEach( user -> System.out.println(""+ user));
    }
    
}
