/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.techbrothers.techsolutions.springbootjpamultipledatasources.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.Table;

/**
 *
 * @author chiru
 */
@Entity
@Table(name = "table2")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "procedure2", procedureName = "new_procedure", resultClasses = User2.class)
})
public class User2 implements Serializable{

       
    @Id
    private String name;

    @Override
    public String toString() {
        return "User2{" + "name=" + name + ", age=" + age + '}';
    }
    
    private String age;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
}
